<?php
    //panggil file untuk koneksi ke DB
    require_once 'class_dbkoneksi.php';
    //buat class
    class DAO{
      private $tableName="";
      private $koneksi= null;

      public function __construct($nama_table)
      {
        $this->tableName=$nama_table;
        $database = new DBKoneksi();
        $this->koneksi = $database->getKoneksi();
      }

      public function getAll(){
        $sql = "SELECT *FROM " .$this->tableName;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
      }

      public function hapus($pk){
        $sql = "DELETE FROM " . $this->tableName . " WHERE id=?";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute([$pk]);
        return $ps->rowCount();
    }

    public function findByID($pk){
        $sql = "SELECT * FROM " . $this->tableName . " WHERE id=?";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute([$pk]);
        return $ps->fetch();
    }

    public function masuk($pk){
      $sql = "INSERT INTO " . $this->tableName .
      "id, kode, judul,deskripsi, narasumber, kategori_id, biaya, kapasitas, tgl_mulai, tgl_akhir, tempat".
      "VALUES(default,?,?,?,?,?,?,?,?,?,?)";
      $ps = $this->koneksi->prepare($sql);
      $ps->execute([$pk]);
      return $ps->rowCount();
    }
}

?>
